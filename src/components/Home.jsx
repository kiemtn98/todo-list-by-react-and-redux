import React, {useState, useEffect} from "react";
import Create from "./Create";
import SearchAndSort from "./SearchAndSort/SearchAndSort";
import Tasks from "./Tasks/Tasks";
import {KEY_LIST_TASK} from "../constant/state";
import _ from "lodash";

export default function Home() {

    const initialFilter = {
        gender: -1,
        salary: -1
    };

    const initialSort = {
        orderBy: 1,
        orderValue: 1
    };

    const [tasks, setTask] = useState([]);
    const [search, setSearch] = useState('');
    const [sort, setSort] = useState(initialSort);
    const [filter, setFilter] = useState(initialFilter);

    useEffect(() => {
        const tasks = getTaskInLS();
        setTask(tasks);
    }, []);

    const getTaskInLS = () => {
        const tasksInLS = localStorage.getItem(KEY_LIST_TASK);
        return tasksInLS !== null ? JSON.parse(tasksInLS) : [];
    };

    const onActionTask = task => {
        if (task.id === -1) {
            delete task.index;
            task.id = _.random(1, 10000);
            tasks.unshift(task);
            return saveDataAndSaveLS([...tasks]);
        }
        return updateTask(task);

    };

    const onDeleteTask = taskId => {
        const index = _.findIndex(tasks, task => {
            return task.id === taskId
        });

        if (index !== -1) {
            tasks.splice(index, 1);
        }
        saveDataAndSaveLS([...tasks]);
    };

    const saveDataAndSaveLS = tasks => {
        setTask(tasks);
        localStorage.setItem(KEY_LIST_TASK, JSON.stringify(tasks));
    };

    const updateTask = taskEdit => {
        const index = _.findIndex(tasks, task => {
            return task.id === taskEdit.id
        });
        tasks.splice(index, 1, taskEdit);
        saveDataAndSaveLS([...tasks]);
    };

    const onFilter = filter => {
        setFilter(filter);
        let listTaskFilter = filterGender(filter.gender);
        listTaskFilter = filterSalary(filter.salary, listTaskFilter);
        setTask([...listTaskFilter]);
    };

    const filterGender = (gender) => {
        const listTask = getTaskInLS();

        if (gender === -1) {
            return listTask;
        }

        return _.filter(listTask, task => {
            return task.gender === gender
        });

    };

    const filterSalary = (salary, listTask) => {
        switch (salary) {
            case -1:
                return [...listTask];

            case 0:
                return _.filter(listTask, task => {
                    return task.salary < 5000000;
                });

            case 1:
                return _.filter(listTask, task => {
                    return task.salary >= 5000000 && task.salary < 10000000;
                });

            default:
                return _.filter(listTask, task => {
                    return task.salary >= 10000000;
                });
        }
    };

    const onSearch = keyword => {
        setSearch(keyword);
        const listTask = searchString(getTaskInLS(), keyword);
        setTask(listTask);
    };

    const searchString = (tasks, keyword) => {
        return _.filter(tasks, task => {
            const taskName = task.taskName.toUpperCase();
            const address = task.address.toUpperCase();
            return taskName.indexOf(keyword.toUpperCase()) !== -1
                || address.indexOf(keyword.toUpperCase()) !== -1
        });
    };

    const onSort = sort => {
        setSort(sort);
        const listSortTask = sortTask(getTaskInLS(), sort);
        setTask(listSortTask);
    };

    const sortTask = (tasks, sort) => {
        if (sort.orderBy === 1 && sort.orderValue === 1) {
            return _.orderBy(tasks, ['taskName'], ['asc'])
        }

        if (sort.orderBy === 1 && sort.orderValue === -1) {
            return _.orderBy(tasks, ['taskName'], ['desc'])
        }

        if (sort.orderBy === 2 && sort.orderValue === 1) {
            return _.orderBy(tasks, ['salary'], ['asc'])
        }

        return _.orderBy(tasks, ['salary'], ['desc'])

    };

    return (
        <div className="container mg-top-5pc">
            <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header text-center alert-info font-weight-bold">
                            Quản lý công việc
                        </div>
                    </div>
                </div>
            </div>

            <div className="row mg-top-2pc">
                <Create task={(task) => {
                    onActionTask(task)
                }}/>
            </div>

            <div className="row mg-top-2pc">
                <SearchAndSort search={search}
                               keyword={keyword => {
                                   onSearch(keyword)
                               }}
                               initialSort={sort}
                               sort={sort => {onSort(sort)}}
                />
            </div>

            <div className="row mg-top-2pc">
                <Tasks tasks={tasks}
                       newTask={task => {
                           onActionTask(task)
                       }}
                       deleteTask={taskId => {
                           onDeleteTask(taskId)
                       }}
                       filter={filter => {
                           onFilter(filter)
                       }}
                       initialFilter={filter}
                />
            </div>
        </div>
    )
}
