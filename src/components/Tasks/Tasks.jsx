import React, {useState, useEffect} from "react";
import {Table} from "react-bootstrap";
import TaskItem from "./TaskItem";
import {Form} from "react-bootstrap";
import {genderFilter, salaryFilter} from "../../constant/define";

export default function Tasks(props) {
    const initialFilter = {
        gender: -1,
        salary: -1
    };

    const [tasks, setTask] = useState([]);
    const [filter, setFilter] = useState(initialFilter);

    useEffect(() => {
        setTask(props.tasks);
        setFilter(props.initialFilter);
    }, [props]);

    const taskItem = tasks.map((task, index) => {
        return <TaskItem key={index}
                         newTask={task => {
                             onUpdateItem(task)
                         }}
                         deleteTask={taskId => {
                             onDeleteItem(taskId)
                         }}
                         task={task} index={index}/>
    });

    const onUpdateItem = task => {
        props.newTask(task);
    };

    const onDeleteItem = taskId => {
        props.deleteTask(taskId);
    };

    const onChangeFilter = e => {
        const { name, value } = e.target;

        const gender = name === 'gender' ? parseInt(value) : filter.gender;

        const salary = name === 'salary' ? parseInt(value) : filter.salary;

        const changeFilter = {
            salary: salary,
            gender: gender
        };

        setFilter(changeFilter);

        props.filter(changeFilter);
    };


    const selectGender = genderFilter.map((gender, index) => {
       return <option value={index - 1} key={index}>{gender}</option>;
    });

    const selectSalary = salaryFilter.map((salary, index) => {
        return <option value={index - 1} key={index}>{salary}</option>;
    });

    return (
        <div className="col-md-12">
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th className="text-center">STT</th>
                    <th>Tên công việc</th>
                    <th>Địa chỉ</th>
                    <th>Giới tính yêu cầu</th>
                    <th className="text-right">Mức lương</th>
                    <th className="text-center">Thao tác</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colSpan={3}/>
                    <td>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Control as="select" name="gender" onChange={onChangeFilter}>
                                { selectGender }
                            </Form.Control>
                        </Form.Group>
                    </td>
                    <td>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Control as="select" name="salary" onChange={onChangeFilter}>
                                { selectSalary }
                            </Form.Control>
                        </Form.Group>
                    </td>
                    <td/>
                </tr>
                {taskItem}
                </tbody>
            </Table>
        </div>
    )
}
