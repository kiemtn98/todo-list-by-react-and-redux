import React, {useState} from "react";
import {Button} from "react-bootstrap";
import ModalAction from "../Modal/ModalAction";
import NumberFormat from "react-number-format";
import ModalDelete from "../Modal/ModalDelete";

export default function TaskItem(props) {
    const [show, setShow] = useState(false);

    const toggleModal = (isToggle) => {
        if (isToggle) {
            return setShow(!show);
        }
        return setShow(false);
    };

    const { task } = props;

    const gender = () => {
        if (task.gender === 1) {
            return 'Nam';
        }

        if (task.gender === 2) {
            return 'Nữ';
        }

        return 'Không yêu cầu';
    };

    const onUpdateItem = task => {
        props.newTask(task);
    };

    const onDeleteItem = taskId => {
        props.deleteTask(taskId)
    };

    return (
        <React.Fragment>
            <tr>
                <td className="text-center">{ props.index + 1 }</td>
                <td>{ task.taskName }</td>
                <td>{ task.address }</td>
                <td>{ gender() }</td>
                <td className="text-right">
                    <NumberFormat value={ task.salary } suffix={' VNĐ'} displayType={'text'} thousandSeparator={true} />
                </td>
                <td className="text-center">
                    <Button variant="warning" onClick={() => toggleModal(true)}>
                        <i className="fas fa-edit" />
                    </Button>{'  '}
                    <ModalAction show={show}
                                 index={props.index}
                                 task={task}
                                 newTask={task => {onUpdateItem(task)}}
                                 title="Cập nhật công việc"
                                 button="Cập nhật"
                                 onHide={() => {toggleModal(false)}}
                    />
                    <ModalDelete task={task} deleteTask={taskId => {onDeleteItem(taskId)}}/>
                </td>
            </tr>
        </React.Fragment>
    )
}
