import React, {useState} from "react";
import {Button} from "react-bootstrap";
import ModalAction from "./Modal/ModalAction";

export default function Create(props) {

    const [show, setShow] = useState(false);

    const toggleModal = (isToggle) => {
        if (isToggle) {
            return setShow(!show);
        }
        return setShow(false);
    };

    const onCreateTask = task => {
        props.task(task)
    };

    return (
        <div className="col-md-12">
            <Button variant="primary" onClick={() => {
                toggleModal(true)
            }}>
                <i className="fas fa-plus"/>{''} Thêm mới công việc
            </Button>
            <ModalAction show={show}
                         newTask={(task) => {
                             onCreateTask(task)
                         }}
                         title="Thêm mới công việc"
                         button="Thêm mới"
                         onHide={() => {
                             toggleModal(false)
                         }}
            />
        </div>
    )
}
