import React, {useState, useEffect} from "react";
import {Dropdown} from "react-bootstrap";

export default function Sort(props) {
    const initialSort = {
        orderBy: 1,
        orderValue: 1
    };

    const [sort, setSort] = useState(initialSort);

    useEffect(() => {
        setSort(props.initialSort)
    }, [props]);

    const items = [
        {
            className: 'fas fa-sort-alpha-up',
            name: 'Tên công việc A - Z',
            orderBy: 1,
            orderValue: 1
        },
        {
            className: 'fas fa-sort-alpha-down',
            name: 'Tên công việc Z - A',
            orderBy: 1,
            orderValue: -1
        },
        {
            className: 'fas fa-sort-numeric-up',
            name: 'Mức lương tăng dần',
            orderBy: 2,
            orderValue: 1
        },
        {
            className: 'fas fa-sort-numeric-down',
            name: 'Mức lương giảm dần',
            orderBy: 2,
            orderValue: -1
        }
    ];

    const onChangeSort = sort => {
        setSort(sort);
        props.sort(sort);
    };

    const dropdownItem = items.map((item, index) => {
        const active = sort.orderBy === item.orderBy
            && sort.orderValue === item.orderValue;

        const sortValue = {
            orderBy: item.orderBy,
            orderValue: item.orderValue
        };

        return <Dropdown.Item key={index} active={active}
                              onClick={() => onChangeSort(sortValue)}
        >
            <i className={item.className}/> {' ' + item.name}
        </Dropdown.Item>
    });

    return (
        <div className="col-md-3">
            <Dropdown>
                <Dropdown.Toggle variant="warning" id="dropdown-basic">
                    Sắp xếp {'  '}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    {dropdownItem}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    )
}
