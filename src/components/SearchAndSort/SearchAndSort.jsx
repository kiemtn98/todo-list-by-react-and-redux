import React, {useState} from "react";

import Search from "./Search";
import Sort from "./Sort";

export default function SearchAndSort(props) {

    const onSearch = keyword => {
        props.keyword(keyword);
    };

    const onSort = sort => {
        props.sort(sort);
    };

    return (
        <React.Fragment>
            <Search search={props.search} keyword={keyword => {onSearch(keyword)}}/>
            <Sort initialSort={props.initialSort} sort={sort => {onSort(sort)}}/>
        </React.Fragment>
    )
}
