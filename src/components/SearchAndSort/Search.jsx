import React, {useState, useEffect} from "react";
import {Button, FormControl, InputGroup} from "react-bootstrap";

export default function Search(props) {
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        setKeyword(props.search);
    }, [props.search]);

    const onChangeValue = e => {
        setKeyword(e.target.value);
    };

    const onSearch = () => {
        props.keyword(keyword);
    };

    return (
        <div className="col-md-8">
            <InputGroup className="mb-3">
                <FormControl
                    placeholder="Nhập tên công việc, địa chỉ,..."
                    aria-label="Nhập tên công việc, địa chỉ,..."
                    aria-describedby="basic-addon2"
                    value={keyword}
                    onChange={onChangeValue}
                />
                <InputGroup.Append>
                    <Button variant="success" onClick={onSearch}>
                        <i className="fas fa-search"/> {' '}
                        Tìm kiếm
                    </Button>
                </InputGroup.Append>
            </InputGroup>

        </div>
    )
}
