import React, {useState} from "react";
import { Button, Modal } from "react-bootstrap";

export default function ModalDelete(props) {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const { task } = props;

    const acceptDelete = () => {
        props.deleteTask(task.id);
        handleClose();
    };

    return (
        <React.Fragment>
            <Button variant="danger" onClick={handleShow}>
                <i className="fas fa-trash-alt" />
            </Button>

            <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Xóa công việc {task.taskName}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Bạn có chắc chắn muốn xóa công việc
                    {' ' + task.taskName + ' '}
                    ?</Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleClose}>
                        Hủy bỏ
                    </Button>
                    <Button variant="danger" onClick={acceptDelete}>
                        Xóa
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    );
}
