import React, {useState, useEffect} from "react";
import {Modal, Button, Form, Col, Row} from "react-bootstrap";
import NumberFormat from "react-number-format";
import {TASK_NAME, ADDRESS, GENDER, SALARY} from "../../constant/state";

export default function ModalAction(props) {
    const [taskName, setTaskName] = useState(TASK_NAME);
    const [address, setAddress] = useState(ADDRESS);
    const [gender, setGender] = useState(GENDER);
    const [salary, setSalary] = useState(SALARY);

    useEffect(() => {
        if (props.button === 'Cập nhật') {
            setTaskName(props.task.taskName);
            setAddress(props.task.address);
            setGender(props.task.gender);
            setSalary(props.task.salary);
        }
    }, [props]);

    const onChangeValue = e => {
        const {name, value} = e.target;
        if (name === 'taskName') {
            return setTaskName(value);
        }

        if (name === 'address') {
            return setAddress(value);
        }

        if (name === 'gender') {
            return setGender(parseInt(value));
        }

        if (name === 'salary' && value > 0) {
            return setSalary(parseInt(value));
        }
    };

    const clearData = () => {
        setTaskName(TASK_NAME);
        setAddress(ADDRESS);
        setGender(GENDER);
        setSalary(SALARY);
    };

    const onSubmitForm = e => {
        e.preventDefault();
        const task = {
            taskName: taskName || 'Công việc mới',
            address: address || 'Hưng Yên',
            gender: gender,
            salary: salary,
            id: -1
        };

        if (props.button === 'Cập nhật') {
            task.id = props.task.id;
        }

        props.newTask(task);
        props.onHide();
        clearData();
    };

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Form onSubmit={onSubmitForm}>
                <Modal.Header closeButton className="text-center">
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            Tên công việc
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control name="taskName" type="text" placeholder="Tên công việc"
                                          value={taskName} onChange={onChangeValue} required
                            />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalPassword">
                        <Form.Label column sm={2}>
                            Địa chỉ
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control
                                name="address" onChange={onChangeValue}
                                value={address}
                                required
                                type="text" placeholder="Địa chỉ"/>
                        </Col>
                    </Form.Group>
                    <fieldset>
                        <Form.Group as={Row}>
                            <Form.Label as="legend"
                                        column sm={2}
                            >
                                Giới tính
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Check
                                    inline
                                    type="radio"
                                    label="Nam"
                                    name="gender"
                                    id="formHorizontalRadios1"
                                    value={1}
                                    checked={gender === 1}
                                    onChange={onChangeValue}
                                />
                                <Form.Check
                                    inline
                                    type="radio"
                                    label="Nữ"
                                    name="gender"
                                    id="formHorizontalRadios2"
                                    value={2}
                                    checked={gender === 2}
                                    onChange={onChangeValue}
                                />
                                <Form.Check
                                    inline
                                    type="radio"
                                    label="Không yêu cầu"
                                    name="gender"
                                    id="formHorizontalRadios3"
                                    value={0}
                                    checked={gender === 0}
                                    onChange={onChangeValue}
                                />
                            </Col>
                        </Form.Group>
                    </fieldset>
                    <Form.Group as={Row} controlId="formHorizontalCheck">
                        <Form.Label column sm={2}>
                            Mức lương
                        </Form.Label>
                        <Col sm={10}>
                            <NumberFormat
                                className="form-control"
                                onChange={onChangeValue}
                                value={salary}
                                name="salary"
                                placeholder="Mức lương"
                            />
                        </Col>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" type="submit">{props.button}</Button>
                    <Button variant="danger" onClick={props.onHide}>Đóng</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}
