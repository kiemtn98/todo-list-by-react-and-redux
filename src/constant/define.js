export const genderFilter = [
    'Tất cả giới tính',
    'Không phân biệt',
    'Giới tính nam',
    'Giới tính nữ'
];

export const salaryFilter = [
    'Tất cả mức lương',
    'Nhỏ hơn 5 triệu',
    'Từ 5 - 10 triệu',
    'Lớn hơn 10 triệu'
];
